# rt-twitterbot

Automated bot that retweets someone's Twitter account with a specific comment, everytime that account tweets. Retweets from that account not included.

## Dependencies
* [Tweepy](https://www.tweepy.org/)